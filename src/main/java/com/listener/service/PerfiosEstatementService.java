package com.listener.service;

import com.cashe.persistence.models.CustomerDocuments;

public interface PerfiosEstatementService {
	public String generatePerfiosEstatementTransaction(Long CustomerId) throws Exception;
	public void downloadBankStatements(Long customerId,String perfiosTransactionId);
	public void uploadDocumentToPerfios(String perfiosTransactionId,CustomerDocuments customerDocument);
	public void completePerfiosTransaction(String perfiosTransactionId) throws Exception;
	public void transactionStatus(String perfiosTransactionId) throws Exception;
}
