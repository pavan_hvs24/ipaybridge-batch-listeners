package com.listener.service;

import com.amazonaws.auth.AWSCredentialsProvider;

@FunctionalInterface
public interface AwsCredentialsInterface extends AWSCredentialsProvider {

	@Override
	public default void refresh(){
		System.out.println("No Implementation required as of now");
	}
}
