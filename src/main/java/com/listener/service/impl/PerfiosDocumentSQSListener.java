package com.listener.service.impl;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Stream;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.cashe.persistence.enums.DocumentTypesEnum;
import com.cashe.persistence.models.CustomerDocuments;
import com.listener.entity.PerfiosEstatementReport;
import com.listener.entity.PerfiosEstatementTransactions;
import com.listener.repository.CustomerDocumentsRepository;
import com.listener.repository.CustomersRepository;
import com.listener.repository.PerfiosEstatementRepository;
import com.listener.repository.PerfiosEstatmentTxnRepository;
import com.listener.repository.PerfiosInstitutionRepository;
import com.listener.service.PerfiosEstatementService;
import com.listener.service.S3FileService;
import com.listener.util.PerfiosUtility;

@Service
public class PerfiosDocumentSQSListener implements MessageListener,PerfiosEstatementService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PerfiosDocumentSQSListener.class);
	private final int TXN_CODE_LENGTH = 16;

	@Autowired 
	private PerfiosUtility perfiosUtility;

	@Autowired 
	private  PerfiosEstatmentTxnRepository perfiosEstatementTxnDao;

	@Autowired
	private CustomersRepository customersRepository;

	@Autowired 
	private S3FileService s3FileService;

	@Autowired
	private CustomerDocumentsRepository customerDocumentsRepository;

	@Autowired
	private PerfiosEstatementRepository perfiosEstatementRepository;

	@Autowired
	private PerfiosInstitutionRepository perfiosInstitutionRepository;

	@Value("${api.version}")
	private String apiVersion;
	@Value("${transaction.complete.callback.url}")
	private String transactionUrl;
	@Value("${perfios.payload.signature.url}")
	private String payloadSignatureUrl;
	@Value("${perfios.loantype}")
	private String perfiosLoanType;
	@Value("${perfios.start.transaction.url}")
	private String startTransactionUrl;
	@Value("${perfios.vendor.id}")
	private String vendorId;
	@Value("${bankstatement.download.path}")
	private String bankStatementDownloadPath;
	@Value("${perfios.upload.url}")
	private String perfiosUploadUrl;
	@Value("${perfios.complete.txn.url}")
	private String perfiosTxnCompleteUrl;
	@Value("${perfios.sideline.queue.name}")
	private String perfiosSideLineQueue;
	@Value("${queue.endpoint}")
	private String endpoint;
	@Value("${perfios.txn.status.url}")
	private String perfiosTxnStatusUrl;	
	@Value("${perfios.txn.retrive.report.url}")
	private String perfiosTxnRetrieveReportUrl;

	@Autowired
	private AmazonSQS amazonSqs;


	public void onMessage(Message message) {
		TextMessage textMessage = (TextMessage) message;
		try {
			LOGGER.info("Received message "+ textMessage.getText());
			JSONParser parser = new JSONParser();
			JSONObject obj=(JSONObject)parser.parse(textMessage.getText());
			Long customerId=new Long(obj.get("customer_id").toString());
			/*	Long abc=perfiosInstitutionRepository.getInstiutionId(customerId);
			System.out.println(abc);*/

			String perfiosTransactionId=generatePerfiosEstatementTransaction(customerId);
			downloadBankStatements(customerId,perfiosTransactionId);
			completePerfiosTransaction(perfiosTransactionId);
			transactionStatus(perfiosTransactionId);
			transactionReport(perfiosTransactionId);
			
		} catch (JMSException e) {
			try {
				pushToSlq(((TextMessage) message).getText());
			} catch (JMSException e1) {
				e1.printStackTrace();
			}
			LOGGER.error("Error processing message ",e);
		} catch (Exception e) {
			try {
				pushToSlq(((TextMessage) message).getText());
			} catch (JMSException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();

		}
	}
	@Override
	public String generatePerfiosEstatementTransaction(Long customerId) throws Exception{
		String casheTxnId = RandomStringUtils.randomAlphanumeric(TXN_CODE_LENGTH).toUpperCase();
		Long id=perfiosInstitutionRepository.getInstiutionId(customerId);
		System.out.println(id);

		String payload =perfiosUtility.getStartTransactionPayload(apiVersion,vendorId,casheTxnId,"1000",perfiosLoanType,24,transactionUrl,id);
		String 	payloadForSignature = payload.replaceAll("\n", "");
		System.out.println("payloadForSignature ::  "+payloadForSignature);
		System.out.println("payload ::  "+ payload);
		//System.out.println(customersRepository.findOne(customerId));
		PerfiosEstatementTransactions perfiosEstatemntTransactions= PerfiosEstatementTransactions.builder()
				.casheTxnId(casheTxnId)
				.createdDate(new Timestamp(System.currentTimeMillis()))
				.updatedDate(new Timestamp(System.currentTimeMillis()))
				.customerId(customerId)
				.updatedBy(customerId)
				.requestPayload(payload)
				.build();
		perfiosEstatementTxnDao.save(perfiosEstatemntTransactions);
		String finalSignature =  com.cashe.perfios.service.impl.PerfiosUtility.getPerfiosSignatureForPayload(payloadForSignature,payloadSignatureUrl);
		String errorResponse="";
		String successResponsePayload="";
		HttpResponse response=perfiosUtility.postPayloadToPerfios(startTransactionUrl,payload,finalSignature);
		if(response.getStatusLine().getStatusCode()==200){
			ResponseHandler<String> handler = new BasicResponseHandler();
			successResponsePayload=handler.handleResponse(response);
		}else{
			for(int i=0;i<response.getEntity().getContentLength();i++){
				errorResponse+=Character.toString((char)response.getEntity().getContent().read());
			}
			System.out.println(errorResponse);
		}
		String perfiosTransactionId=getPerfiosTransactionId(successResponsePayload);
		System.out.println(perfiosTransactionId);
		PerfiosEstatementTransactions estatementTxn=perfiosEstatementTxnDao.findByCasheTxnId(casheTxnId);
		estatementTxn.setResponsePayload(successResponsePayload);
		estatementTxn.setPerfiosTxnId(perfiosTransactionId);
		estatementTxn.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
		perfiosEstatementTxnDao.save(estatementTxn);
		return perfiosTransactionId;
	}

	private String getPerfiosTransactionId(String responsePayload) throws XPathExpressionException {
		XPathFactory xpf = XPathFactory.newInstance();
		XPath xpath = xpf.newXPath();
		return (String)xpath.evaluate("/Success/perfiosTransactionId", new InputSource(new StringReader(responsePayload)), XPathConstants.STRING);
	}
	@Override
	public void downloadBankStatements(Long customerId,String perfiosTransactionId) {
		List<CustomerDocuments> customerDocuments=customerDocumentsRepository.findByIsActiveAndDocumentTypeAndCustomerIdAndIsPdf(1,DocumentTypesEnum.getDocTypeByName(DocumentTypesEnum.getDocTypeNameByID(DocumentTypesEnum.BANK_STATEMENT.getValue())),customerId,1);
		customerDocuments.forEach(customerDocument->{
			s3FileService.downloadFile(bankStatementDownloadPath+"/"+Stream.of(customerDocument.getCustomerDocumentName().split("/")).reduce((first,last)->last).get(),customerDocument.getCustomerDocumentName());
			uploadDocumentToPerfios(perfiosTransactionId,customerDocument);
		});
	}
	@Override
	public void uploadDocumentToPerfios(String perfiosTransactionId,CustomerDocuments customerDocument) {
		String errorResponse="",successResponse="";
		try {
			String filename=Stream.of(customerDocument.getCustomerDocumentName().split("/")).reduce((first,last)->last).get();
			HttpResponse response=perfiosUtility.uploadDocumentToPerfios(bankStatementDownloadPath+"/"+filename,perfiosUploadUrl,perfiosTransactionId,customerDocument.getPdfPassword(),vendorId);
			if(response.getStatusLine().getStatusCode()==200){
				ResponseHandler<String> handler = new BasicResponseHandler();
				successResponse=handler.handleResponse(response);
			}else{
				for(int i=0;i<response.getEntity().getContentLength();i++){
					errorResponse+=Character.toString((char)response.getEntity().getContent().read());
				}
				System.out.println(errorResponse);
			}
			updateEstatementResponse(perfiosTransactionId,successResponse,errorResponse,customerDocument);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void updateEstatementResponse(String perfiosTransactionId, String successResponse, String errorResponse,CustomerDocuments customerDocument) {
		PerfiosEstatementReport perfiosEstatementReport= PerfiosEstatementReport.builder()
				.perfiosTxnId(perfiosTransactionId)
				.customerDocumentId(customerDocument.getCustomerDocumentId())
				.createdDate(new Timestamp(System.currentTimeMillis()))
				.updatedDate(new Timestamp(System.currentTimeMillis()))
				.successResponsePayload(successResponse)
				.errorResponsePayload(errorResponse)
				.updatedBy(customerDocument.getCustomerId())
				.build();
		perfiosEstatementRepository.save(perfiosEstatementReport);
	}
	@Override
	public void completePerfiosTransaction(String perfiosTransactionId) throws Exception {
		String errorResponse="";
		String successResponse="";
		try {
			String payload=perfiosUtility.getCompleteTransactionPayload(apiVersion, vendorId, perfiosTransactionId);
			String 	payloadForSignature = payload.replaceAll("\n", "");
			String finalSignature =  com.cashe.perfios.service.impl.PerfiosUtility.getPerfiosSignatureForPayload(payloadForSignature,payloadSignatureUrl);
			System.out.println("finalSignature"+finalSignature);
			System.out.println("payload"+payload);
			System.out.println("perfiosTxnCompleteUrl"+perfiosTxnCompleteUrl);
			HttpResponse response=perfiosUtility.postPayloadToPerfios(perfiosTxnCompleteUrl,payload,finalSignature);
			if(response.getStatusLine().getStatusCode()==200){
				ResponseHandler<String> handler = new BasicResponseHandler();
				successResponse=handler.handleResponse(response);
			}
			else{
				for(int i=0;i<response.getEntity().getContentLength();i++){
					errorResponse+=Character.toString((char)response.getEntity().getContent().read());
				}
				System.out.println(errorResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void transactionStatus(String perfiosTransactionId) throws Exception {
		String txnErrorResponse="";
		String txnSuccessResponse="";
		try {
			PerfiosEstatementTransactions perfiosEstatementTransactions=perfiosEstatementTxnDao.findByPerfiosTxnId(perfiosTransactionId);
			String txnPayload=perfiosUtility.getTxnStatusPayload(apiVersion, vendorId, perfiosEstatementTransactions.getCasheTxnId());
			String 	txnPayloadForSignature = txnPayload.replaceAll("\n", "");
			String txnSignature =  com.cashe.perfios.service.impl.PerfiosUtility.getPerfiosSignatureForPayload(txnPayloadForSignature,payloadSignatureUrl);

			HttpResponse txnResponse=perfiosUtility.postPayloadToPerfios(perfiosTxnStatusUrl,txnPayload,txnSignature);
			if(txnResponse.getStatusLine().getStatusCode()==200){
				ResponseHandler<String> handler = new BasicResponseHandler();
				txnSuccessResponse=handler.handleResponse(txnResponse);
				System.out.println("txnSuccessResponse in transactionStatus"+txnSuccessResponse);
			}
			else{
				for(int i1=0;i1<txnResponse.getEntity().getContentLength();i1++){
					txnSuccessResponse+=Character.toString((char)txnResponse.getEntity().getContent().read());
				}
				System.out.println("txnErrorResponse in transactionStatus"+txnErrorResponse);
			}
			String txnStatus=getTxnStatus(txnSuccessResponse);
			//PerfiosEstatementTransactions perfiosEstatementTxn=perfiosEstatementTxnDao.findByPerfiosTxnId(perfiosTransactionId);
			perfiosEstatementTransactions.setTxnCompleteResponse(txnSuccessResponse!=null ?txnSuccessResponse:null);
			perfiosEstatementTransactions.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
			perfiosEstatementTransactions.setTxnStatus((txnStatus.equalsIgnoreCase("success"))?true:false); //v are getting pending status too
			perfiosEstatementTxnDao.save(perfiosEstatementTransactions);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void transactionReport(String perfiosTransactionId) throws Exception {
		String txnErrorResponse="";
		String txnSuccessResponse="";
		try {
			PerfiosEstatementTransactions perfiosEstatementTransactions=perfiosEstatementTxnDao.findByPerfiosTxnId(perfiosTransactionId);
			String txnReportPayload=perfiosUtility.getTxnReportRetrievePayload(apiVersion, vendorId,  perfiosEstatementTransactions.getPerfiosTxnId(), perfiosEstatementTransactions.getCasheTxnId());
			String 	txnPayloadForSignature = txnReportPayload.replaceAll("\n", "");
			String txnSignature =  com.cashe.perfios.service.impl.PerfiosUtility.getPerfiosSignatureForPayload(txnPayloadForSignature,payloadSignatureUrl);

			HttpResponse txnResponse=perfiosUtility.postPayloadToPerfios(perfiosTxnRetrieveReportUrl,txnReportPayload,txnSignature);
			if(txnResponse.getStatusLine().getStatusCode()==200){
				ResponseHandler<String> handler = new BasicResponseHandler();
				txnSuccessResponse=handler.handleResponse(txnResponse);
				System.out.println("txnSuccessResponse in transactionReport"+txnSuccessResponse);
			}
			else{
				for(int i1=0;i1<txnResponse.getEntity().getContentLength();i1++){
					txnSuccessResponse+=Character.toString((char)txnResponse.getEntity().getContent().read());
				}
				System.out.println("txnErrorResponse in transactionReport"+txnErrorResponse);
			}
			String txnStatus=getTxnStatus(txnSuccessResponse);
			/*//PerfiosEstatementTransactions perfiosEstatementTxn=perfiosEstatementTxnDao.findByPerfiosTxnId(perfiosTransactionId);
			perfiosEstatementTransactions.setTxnCompleteResponse(txnSuccessResponse!=null ?txnSuccessResponse:null);
			perfiosEstatementTransactions.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
			perfiosEstatementTransactions.setTxnStatus((txnStatus.equalsIgnoreCase("success"))?true:false); //v are getting pending status too
			perfiosEstatementTxnDao.save(perfiosEstatementTransactions);*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getTxnStatus(String responsePayload) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {

		ByteArrayInputStream bais = new ByteArrayInputStream(responsePayload.getBytes());
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document dom = db.parse(bais);
		XPath xpf = XPathFactory.newInstance().newXPath();
		
		// Parsing txn status
		XPathExpression expr = xpf.compile("//Part");
		String txnStatus = null;
		NodeList listOfiNodes = (NodeList) expr.evaluate(dom, XPathConstants.NODESET);
		for (int i = 0; i < listOfiNodes.getLength(); i++) {
			Node item = listOfiNodes.item(i);
			if (null != item && item.getAttributes().getNamedItem("status")!=null) {
				txnStatus = item.getAttributes().getNamedItem("status").getTextContent();
			}
		}
		return txnStatus;
	}
	
	private void pushToSlq(String msg){
		SendMessageRequest send_msg_request;
		send_msg_request = new SendMessageRequest()
		.withQueueUrl(endpoint+perfiosSideLineQueue)
		.withMessageBody(msg)
		.withDelaySeconds(5);
		amazonSqs.sendMessage(send_msg_request);
	}

}