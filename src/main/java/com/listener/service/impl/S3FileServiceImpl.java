package com.listener.service.impl;

import com.listener.service.S3FileService;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

/**
 *
 * Service that upload a file to provided S3 bucket.
 *
 * Uploads the given file with the specified keyName. KeyName should start with
 * root folder of the bucket.
 *
 * If User want to upload the file to 'reports' folder of the bucket, then
 * keyName should be 'reports/fileName'. If Key contains nested folders
 * 'reports/client/date/fileName' and does not exists on S3 bucket, they will be
 * created automatically.
 *
 */
@Service
public class S3FileServiceImpl implements InitializingBean, S3FileService {

	private static final Logger LOGGER = LoggerFactory.getLogger(S3FileServiceImpl.class);

	@Value("${upload.bucket.name}")
	private String uploadBucketName;

	@Value("${download.bucket.name}")
	private String downloadBucketName;

	@Value("${bucket.region}")
	private String bucketRegion;

	@Value("${aws.access.key.id}")
	private String awsAccessKeyId;

	@Value("${aws.secret.access.key}")
	private String awsSecretAccessKey;

	private AmazonS3 s3Client;

	private AmazonS3 createS3Client() {
		BasicAWSCredentials credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretAccessKey);
		AmazonS3ClientBuilder s3ClientBuilder = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials));
		s3ClientBuilder.setRegion(bucketRegion);
		return s3ClientBuilder.build();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		s3Client = createS3Client();
	}

	@Override
	public void uploadFile(File file, String keyName) {
		try {
			s3Client.putObject(new PutObjectRequest(uploadBucketName, keyName, file));
		} catch (AmazonServiceException serviceException) {
			LOGGER.error("Upload rejected by Amazon S3 due to reason " + serviceException.getMessage(),
					serviceException);

		} catch (AmazonClientException clientException) {
			LOGGER.error("Error occured while communicating to Amazon S3 due to reason " + clientException.getMessage(),
					clientException);

		}
	}

	@Override
	public void downloadFile(String fileLocation, String keyName) {
		S3Object file=null;
		try {
			 file = s3Client.getObject(new GetObjectRequest(downloadBucketName, keyName));
			InputStream inputStream = file.getObjectContent();

			System.out.println("In downloadFile(), fileLocation : "+fileLocation);
			
			Files.copy(inputStream, Paths.get(fileLocation), StandardCopyOption.REPLACE_EXISTING);
			
			System.out.println("In downloadFile(), fileLocation : "+fileLocation+" downloading completed");
		} catch (IOException serviceException) {
			LOGGER.error("Error occured while reading file from S3 from location" + keyName, serviceException);
		}finally{
			try {
				file.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
