package com.listener.service;


import java.io.File;

public interface S3FileService {
	
	public void uploadFile(File file, String keyName);
	public void downloadFile(String fileLocation, String keyName);
}
