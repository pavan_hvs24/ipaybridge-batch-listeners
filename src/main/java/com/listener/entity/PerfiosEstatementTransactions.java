package com.listener.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "perfios_estatement_transactions")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PerfiosEstatementTransactions {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name = "customer_id")
	private Long customerId;

	@Column(name = "cashe_txn_id")
	private String casheTxnId;

	@Column(name = "perfios_txn_id")
	private String perfiosTxnId;

	@Column(name = "perfios_error_code_id")
	private Integer perfiosErrprCodeId;

	@Column(name = "request_for_report")
	private Boolean requestForReport;

	@Column(name = "request_payload")
	private String requestPayload;

	@Column(name = "response_payload")
	private String responsePayload;

	@Column(name = "txn_status")
	private Boolean txnStatus;
	
	@Column(name = "txn_complete_response")
	private String txnCompleteResponse;

	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "updated_date")
	private Timestamp updatedDate;

	@Column(name = "updated_by")
	private Long updatedBy;
}
