package com.listener.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "perfios_institution")
public class PerfiosInstitution {

	
	@Id
	@GeneratedValue
	@Column(name = "perfios_institution_id", unique = true, nullable = false)
	private Long perfiosInstitutionId;

	@Column(name = "address_available")
	private Boolean addressAvailable;


	@Column(name = "form26_as_available")
	private Boolean form26AsAvailable;

	@Column(name = "id")
	private Integer id;
	
	@Column(name = "institution_type")
	private String institutionType;
	
	@Column(name = "itrV_available")
	private Boolean itrVAvailable;
	
	@Column(name = "name")
	private String name;

	@Column(name = "original_statement_available")
	private Boolean originalStatementAvailable;
	
	@Column(name = "bank_code")
	private String bankCode;



}
