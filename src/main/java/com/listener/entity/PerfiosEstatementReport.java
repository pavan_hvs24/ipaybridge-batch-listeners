package com.listener.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "perfios_estatement_report")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PerfiosEstatementReport {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name = "perfios_txn_id")
	private String perfiosTxnId;
	
	
	@Column(name = "success_response_payload")
	private String successResponsePayload;
	
	@Column(name = "error_response_payload")
	private String errorResponsePayload;
	
	@Column(name="customer_document_id")
	private int customerDocumentId;
	
	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "updated_date")
	private Timestamp updatedDate;

	@Column(name = "updated_by")
	private Long updatedBy;
}
