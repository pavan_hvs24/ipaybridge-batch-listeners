package com.listener.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.listener.service.AwsCredentialsInterface;
import com.listener.service.impl.PerfiosDocumentSQSListener;

@Configuration
public class PerfiosDocumentListenerConfiguration {
	@Value("${queue.endpoint}")
	private String endpoint;
	@Value("${perfios.queue.name}")
	private String perfiosQueue;
	@Value("${queue.secret.id}")
	private String secretId;
	@Value("${queue.secret.key}")
	private String secretKey;
	@Value("${perfios.sideline.queue.name}")
	private String perfiosSideLineQueue;

	@Autowired
	private PerfiosDocumentSQSListener sqsListener;

	private AmazonSQS sqs;


	@Bean(name="sqsClient")
	public AmazonSQS getSqsClient(){
		AwsCredentialsInterface awsCredentialsProvider=()->  {return new BasicAWSCredentials(secretId, secretKey);};
		sqs= AmazonSQSClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).withCredentials(awsCredentialsProvider).build();
		return sqs;
	}
	@Bean
	public DefaultMessageListenerContainer jmsListenerContainer() {
		createMissingQueues();
		SQSConnectionFactory sqsConnectionFactory=new SQSConnectionFactory(new ProviderConfiguration(),sqs);
		DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
		dmlc.setConnectionFactory(sqsConnectionFactory);
		dmlc.setDestinationName(perfiosQueue);
		dmlc.setMessageListener(sqsListener);
		return dmlc;
	}
	@Bean
	public JmsTemplate createJMSTemplate() {
		SQSConnectionFactory sqsConnectionFactory=new SQSConnectionFactory(new ProviderConfiguration(),sqs);
		JmsTemplate jmsTemplate = new JmsTemplate(sqsConnectionFactory);
		jmsTemplate.setDefaultDestinationName(perfiosQueue);
		jmsTemplate.setDeliveryPersistent(false);
		return jmsTemplate;
	}
	private void createMissingQueues() {
		if(!checkIfQueueExists(sqs,endpoint+perfiosQueue)){
			createQueue(sqs,perfiosQueue);
		}
		if(!checkIfQueueExists(sqs,endpoint+perfiosSideLineQueue)){
			createQueue(sqs,perfiosSideLineQueue);
		}
	}
	private boolean checkIfQueueExists(AmazonSQS sqs,String queueUrl){
		return sqs.listQueues().getQueueUrls().stream().filter(queue->queue.equals(queueUrl)).findFirst().isPresent();
	}
	private void createQueue(AmazonSQS sqs,String queueName){
		CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName);
		sqs.createQueue(createQueueRequest);
	}
}
