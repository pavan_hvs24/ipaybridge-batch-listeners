package com.listener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
/*@EnableAutoConfiguration(exclude = { 
	    SecurityAutoConfiguration.class 
	})*/
@ComponentScan
@EntityScan(basePackages={"com.cashe.persistence.models","com.listener.entity"})
public class ListenerApplication {
	public static void main(String [] args){
		
        SpringApplication.run(ListenerApplication.class, args);
	}
}
