package com.listener.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.listener.entity.PerfiosEstatementReport;

public interface PerfiosEstatementRepository extends JpaRepository<PerfiosEstatementReport,Long>{

}
