package com.listener.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cashe.persistence.enums.DocumentTypesEnum;
import com.cashe.persistence.models.CustomerDocuments;

public interface CustomerDocumentsRepository extends JpaRepository<CustomerDocuments,Long>{

	public List<CustomerDocuments> findByIsActiveAndDocumentTypeAndCustomerIdAndIsPdf(int isActive, DocumentTypesEnum documentType,Long customerId,int isPdf);
}
