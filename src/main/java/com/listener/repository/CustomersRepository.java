package com.listener.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cashe.persistence.models.Customers;

public interface CustomersRepository extends JpaRepository<Customers,Long>{

}
