package com.listener.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.listener.entity.PerfiosInstitution;

public interface PerfiosInstitutionRepository extends JpaRepository<PerfiosInstitution, Long> {

@Query("SELECT pin.id FROM PerfiosInstitution pin "
		+ "INNER JOIN BankNames bn ON bn.bankCode=pin.bankCode "
		+ "INNER JOIN BankAccountDetail bad ON bad.bankName=bn.bankName "
		+ "WHERE bad.customerId=?1")	
public Long getInstiutionId(Long customerId);	
	
}
