package com.listener.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.listener.entity.PerfiosEstatementTransactions;

public interface PerfiosEstatmentTxnRepository extends JpaRepository<PerfiosEstatementTransactions, Long> {
	
	public PerfiosEstatementTransactions findByCasheTxnId(String casheTxnId);
	
	
	public PerfiosEstatementTransactions findByPerfiosTxnId(String perfiosTxnId);
}
