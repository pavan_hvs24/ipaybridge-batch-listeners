package com.listener.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cashe.persistence.models.CustomerTags;

public interface CustomerTagsRepository extends JpaRepository<CustomerTags,Long>{

}
