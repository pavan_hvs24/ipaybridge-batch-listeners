package com.listener.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class PerfiosUtility {

	private final static Logger LOGGER = Logger.getLogger(PerfiosUtility.class);
	private static void logMessage(String msg){
		LOGGER.debug(msg);
		System.out.println(msg);
	}

	String format = "pdf";

	public static String getPerfiosSignatureForPayload(String payload, String url)  throws Exception {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		StringEntity params =new StringEntity(payload, "UTF-8");
		params.setContentType("*/*");
		post.setEntity(params);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("payload", payload));

		logMessage("Request signature for payload  ( "+payload+") , URL : "+url);

		HttpResponse response = client.execute(post);
		logMessage("Response Status signature for ( "+payload+") : "+response.getStatusLine().getStatusCode());

		if(response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 201) {
			ResponseHandler<String> handler = new BasicResponseHandler();
			String sign = handler.handleResponse(response);
			logMessage("Response signature for ( "+payload+") : "+sign);
			return sign;
		}
		throw new RuntimeException("Signature not generated for requested Payload :"+payload);
	}


	public  String getStartTransactionPayload(String apiVersion,String vendor, String applicationId,String loanAmount,String loanType, int loanDuration, String transactionCompleteCallbackUrl,Long id){ 
		return  "<payload>\n" + "<apiVersion>" + apiVersion + "</apiVersion>\n" +"<vendorId>"
				+ vendor + "</vendorId>\n" + "<txnId>" + applicationId
				+ "</txnId>\n" + "<institutionId>"+id+"</institutionId>\n"
				+"<loanAmount>"+loanAmount+"</loanAmount>\n"
				+"<loanDuration>"+loanDuration+"</loanDuration>\n"
				+"<loanType>"+loanType+"</loanType>\n"
				+"<transactionCompleteCallbackUrl>"
				+ transactionCompleteCallbackUrl + "</transactionCompleteCallbackUrl>\n" + "</payload>";
	}

	public String getCompleteTransactionPayload(String apiVersion,String vendor,String perfiosTransactionId){
		return "<payload>\n" + "<apiVersion>" + apiVersion + "</apiVersion>\n" +"<vendorId>"
				+ vendor + "</vendorId>\n"+"<perfiosTransactionId>"+perfiosTransactionId+"</perfiosTransactionId>\n" + "</payload>";
	}
	
	public String getTxnStatusPayload(String apiVersion,String vendor,String perfiosTransactionId){
		return "<payload>\n" + "<apiVersion>" + apiVersion + "</apiVersion>\n" +"<vendorId>"
				+ vendor + "</vendorId>\n"+"<txnId>"+perfiosTransactionId+"</txnId>\n" + "</payload>";
	}
	
	
	public String getTxnReportRetrievePayload(String apiVersion,String vendor,String perfiosTransactionId,String applicationId) {
		return "<payload>\n" + "<apiVersion>" + apiVersion + "</apiVersion>\n"
				+ "<vendorId>" + vendor + "</vendorId>\n"
				+ "<txnId>" + applicationId + "</txnId>\n"
				+ "<perfiosTransactionId>" + perfiosTransactionId + "</perfiosTransactionId>\n" 
				+ "<reportType>" + format + "</reportType>\n" + "</payload>";
	}

	public HttpResponse postPayloadToPerfios(String statusurl, 
			String payload,String signature) throws Exception {
		System.out.println("Payload "+payload);
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(statusurl);
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("payload", payload));
		urlParameters.add(new BasicNameValuePair("signature", signature));
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		return response;
		/*System.out.println(response.getStatusLine().getStatusCode());
		String errorResponse="";
		for(int i=0;i<response.getEntity().getContentLength();i++){
			errorResponse+=Character.toString((char)response.getEntity().getContent().read());
		}
		System.out.println(errorResponse);
		String errorResponse="";
		String successResponse="";
		if(response.getStatusLine().getStatusCode()==200){
			ResponseHandler<String> handler = new BasicResponseHandler();
			successResponse=handler.handleResponse(response);
			System.out.println("successResponse"+successResponse);
		}else{
			for(int i=0;i<response.getEntity().getContentLength();i++){
				errorResponse+=Character.toString((char)response.getEntity().getContent().read());
			}
			System.out.println(errorResponse);
		}
		ResponseHandler<String> handler = new BasicResponseHandler();
		return handler.handleResponse(response);*/
		
	}

	public HttpResponse uploadDocumentToPerfios(String downloadPath,String uploadUrl,String perfiosTransactionId,String password,String vendorId) throws Exception {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(uploadUrl);
		File fileToUse = new File(downloadPath); 
		FileBody data = new FileBody(fileToUse); 
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addPart("file", data);
		builder.addTextBody("perfiosTransactionId",perfiosTransactionId , ContentType.TEXT_PLAIN);
		builder.addTextBody("vendorId",vendorId , ContentType.TEXT_PLAIN);
		HttpEntity entity=builder.build();
		post.setEntity(entity);
		HttpResponse response = client.execute(post);
		return response;
	}
}